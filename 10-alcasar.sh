#!/usr/bin/env bash
# Alcasar autologin NetworkManager hook
# Mettre dans /etc/NetworkManager/dispatcher.d/ pour se connecter automatiquement
if [[ "$2" == "up" ]] && ( [[ "$1" == "wl"* ]] || [[ "$1" == "enp"* ]] || "$1" == "eth"* ); then
  alcasar
fi
