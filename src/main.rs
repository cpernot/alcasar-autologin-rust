#![feature(iter_intersperse)]
use std::collections::HashMap;

use anyhow::anyhow;
use reqwest::{
    header::{HeaderMap, HeaderName, HeaderValue},
    redirect::Policy,
    Method,
};

// TODO: config file
mod sensitive;

const HOST: &str = "smile-education.fr";
const REQURL: &str = "http://detectportal.firefox.com";

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let client = reqwest::ClientBuilder::new()
        .redirect(Policy::limited(10))
        .build()?;
    let r = client.get(REQURL).send().await?;
    if !r.url().as_str().contains(HOST) {
        println!("Déjà connecté");
        return Ok(());
    }
    let params = r
        .url()
        .query_pairs()
        .map(|(name, arg)| (name.to_string(), arg.to_string()))
        .collect::<HashMap<String, String>>();
    let challenge = params
        .get("challenge")
        .ok_or_else(|| anyhow!("challenge not in params"))?;
    let nasid = params
        .get("nasid")
        .ok_or_else(|| anyhow!("nasid not in params"))?;
    let userurl = params
        .get("userurl")
        .ok_or_else(|| anyhow!("userurl not in params"))?;
    let url = r.url().as_str().to_owned().leak() as &str;
    let headers = HeaderMap::from_iter([
        ("referer", url),
        (
            "accept",
            "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        ),
        (
            "accept-language",
            "fr-FR,en-US;q=0.7,en;q=0.3",
        ),
        (
            "content-type",
            "application/x-www-form-urlencoded",
        ),
    ].into_iter().map(|(name, content)| (HeaderName::from_static(name), HeaderValue::from_static(content))));
    let body = [
        ("challenge", challenge.as_str()),
        ("userurl", userurl.as_str()),
        ("username", sensitive::USERNAME),
        ("password", sensitive::PASSWORD),
        ("button", "Authentification"),
    ]
    .into_iter()
    .map(|(a, b)| format!("{}={}", a, b))
    .intersperse("&".to_owned())
    .collect::<String>();
    let req = client
        .request(Method::POST, format!("https://{}/intercept.php", nasid))
        .headers(headers)
        .body(body);
    let resp = req.send().await?;
    if resp.status().is_success() && !resp.url().as_str().contains("success") {
        println!("Impossible de se connecter");
    } else {
        println!("Connecté");
    }
    Ok(())
}
